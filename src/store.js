import vue from "vue";
import vuex from "vuex";
import axios from "axios";

vue.use(vuex,axios)

export default new vuex.Store({
    state:{
        posts:[]
    },
    getters:{
        postdata:state=>{
            return state.posts;

        }
    },
    actions:{
        loadNews({commit}){

            axios
                .get("https://newsapi.org/v2/everything?q=bitcoin&apiKey=d980dd70569244f589f7536875556d81")
                .then(data =>{
                    let posts=data.data
                    commit('SET_POST',posts)

                })
                .catch(error=>{
                    console.log(error)

                })


        }

    },
    mutations:{

        SET_POST(state,posts){
            state.posts=posts
        },


    }
})
